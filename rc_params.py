import matplotlib.pyplot as pl
import matplotlib

# Tex rendering
# matplotlib.rcParams["text.usetex"] = True
matplotlib.rcParams['font.size'] = 20
matplotlib.rcParams['ps.fonttype'] = 42
pl.rc('font', family='serif')

# Grid
matplotlib.rcParams['grid.linewidth'] = 0.5

# Legend
matplotlib.rcParams['legend.fontsize'] = 15
# matplotlib.rcParams['legend.borderpad'] = 0.1
# matplotlib.rcParams['legend.framealpha'] = 1.
matplotlib.rcParams['legend.fancybox'] = False
matplotlib.rcParams['legend.edgecolor'] = 'black'

# Lines
matplotlib.rcParams['lines.linewidth'] = 2.5
matplotlib.rcParams['lines.markersize'] = 7.
# matplotlib.rcParams['lines.fillstyle'] = 'none'
myfigsize = (12, 8)
