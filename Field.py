"""
Field module will contain a class Field made of Probes objects
"""
import numpy as np

import Probes as prb


class Field_3D():
    """
    This is a Field.
    It contains the spatial information of the probes available.
    We store Probes object in Field Object 
    """

    def __init__(self, file_path=None, temporal_path=None, is_xp=None):
        self.record_file = file_path
        self.temporal_path = temporal_path
        self.list_probe_name = []
        self.probe_dict = {}
        self.probe_hr_mean = None

        # List of variables considered
        self.var_list = ['tempo', 'hr_mean']

        # time domain attributes
        self.td = dict(zip(self.var_list, [None] * len(self.var_list)))

        # frequency domain attributes
        self.fd = dict(zip(self.var_list, [None] * len(self.var_list)))

        # Do shit done
        self.get_probe_HRm()
        self.read_record()

    def read_record(self):
        # Define data type
        my_data_type = np.dtype([('x_position', np.float64), ('y_position', np.float64), ('z_position', np.float64),
                                 ('probe_name', 'S20')])

        # Load data
        record_data = np.loadtxt(self.record_file, dtype=my_data_type, skiprows=0)

        # Get info on the number of probes
        n_lines = np.shape(record_data)
        self.list_probe_name = n_lines

        for i, pb_nb in enumerate(range(n_lines[0])):
            xx = record_data['x_position'][i]
            yy = record_data['y_position'][i]
            zz = record_data['z_position'][i]
            pb_nm = record_data['probe_name'][i]
            print "probe name: \t %s \t %s" % (i + 1, pb_nm)
            # print pb_nm
            bin_path = str(self.temporal_path) + pb_nm + '.ascii'
            # print "bin path in Field class: %s" % bin_path
            self.probe_dict[pb_nm] = prb.AvbpProbe3D(probe_number=i, x_pos=xx, y_pos=yy, z_pos=zz,
                                                     avbp_bin_path=bin_path)

    def get_probe_HRm(self):
        self.probe_hr_mean = prb.ProbeMeanHeatRealise(avbp_bin_path=self.temporal_path)

