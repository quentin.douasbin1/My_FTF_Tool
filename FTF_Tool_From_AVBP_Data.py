import pickle

from Field import *
from rc_params import *


def loadData(from_scratch=False):

    field_short_name = "Mono_test"
    field_full_name = "Field_3D_save_%s.p" % field_short_name

    if from_scratch:
        # Long
        rec_path = './ASCII_RECORD_DATA/probes_Mono2.dat'
        temp_path_monoO2_test = '/Users/qdouasbi/Documents/PYTHON/FTF_MONOINJ/ASCII_PROBE_DATA/'

        field_mono_test = Field_3D(file_path=rec_path, temporal_path=temp_path_monoO2_test)

        # Save Binary Data
        print "Saving Binary Data"
        pickle_dic = {field_short_name: field_mono_test}
        pickle.dump(pickle_dic, open("./BINARY_SAVED_DATA/%s.p" % field_full_name, "wb"))

    else:
        print "Load Binary using Pickle"
        pic = pickle.load(open("./BINARY_SAVED_DATA/%s.p" % field_full_name, "rb"))
        field_mono_test = pic[field_short_name]
        print "Done Loading Binary using Pickle"

    return field_mono_test


def main():
    # Script options --> Load data from txt or bin
    from_scratch = False
    # from_scratch = True

    probe_field_mono_test = loadData(from_scratch)
    probe_field_mono_test.probe_hr_mean.plot_time_domain()
    probe_field_mono_test.probe_hr_mean.plot_frequency_domain()
    probe_field_mono_test.probe_dict['Mono_th000_Probe_038'].plot_time_domain(var_name='press')
    probe_field_mono_test.probe_dict['Mono_th000_Probe_038'].plot_frequency_domain(var_name='press')

    pl.show()


if __name__ == "__main__":
    main()
