"""
This is the probe module
It will contain a class Probe
"""

import numpy as np
import matplotlib.pyplot as pl
from rc_params import *


# TODO: I should collapse everything I can into a generic probe object and the heatRealise and hydrodynamic one should inherit of the methods/attributes

class AvbpProbe3D:
    def __init__(self, probe_number=None, x_pos=None, y_pos=None, z_pos=None, avbp_bin_path=None, zpf=1):
        # Paths
        self.probe_number = probe_number
        self.avbp_bin_path = avbp_bin_path

        # Spatial attributes
        self.x = x_pos
        self.y = y_pos
        self.z = z_pos

        # FFT / FTF parameters
        self.zpf = zpf

        # List of variables considered
        self.var_list = ['tempo', 'vel_u', 'vel_v', 'vel_w', 'press']

        # time domain attributes
        self.td = dict(zip(self.var_list, [None] * len(self.var_list)))

        # frequency domain attributes
        self.fd = dict(zip(self.var_list, [None] * len(self.var_list)))

        # -------------------------
        # Reading the data from file
        self.read_data()

        # Computing frequency domain variables
        self.fill_fd_data()

    def read_data(self):
        data = np.loadtxt(self.avbp_bin_path, skiprows=1)
        data = np.array(data)

        # Storing all data in object
        for i, var in enumerate(self.var_list):
            self.td[var] = data[:, i]

    def fill_fd_data(self):
        for var in self.var_list:
            freq, spec = self.get_fft(var_name=var, zpf=self.zpf)
            self.fd[var] = FFT(freq=freq, spec=spec)

    def get_fft(self, var_name, zpf=1):
        t = self.td['tempo']
        var = self.td[var_name]

        # Sampling rate
        ts = t[1] - t[0]

        # Size of signal
        n_fft = int(t.size * zpf)

        # Hanning correction --> 2.*
        var = 2. * var * np.hanning(len(var))

        # get spectrum
        spec = np.fft.rfft(var, n=n_fft)

        # Normalize properly
        spec /= len(t)

        if False:
            # One sided spectrum ==> spec * 2.
            spec *= 2.

        # get frequency array
        freq = np.fft.rfftfreq(int(zpf * t.size), d=ts)

        return freq, spec

    def __str__(self):
        return "Probe %s, x = %.3f, y = %.3f" % (
            self.probe_number + 1, self.x, self.y)

    def plot_time_domain(self, var_name=None):
        """
        PLot in time domain of variable 'var_name'
        :param var_name: 
        :return: 
        """
        fig, ax = pl.subplots(figsize=myfigsize)
        ax.plot(self.td['tempo'], self.td[var_name], label='%s' % var_name)
        ax.set_xlabel('Time [s]')
        pl.tight_layout()
        ax.legend(loc='best')

    def plot_frequency_domain(self, var_name=None):
        fft_o = self.fd[var_name]

        fig, ax = pl.subplots(figsize=myfigsize)
        ax.semilogy(fft_o.fft_freq, fft_o.fft_mod, label='%s' % var_name)
        ax.set_xlabel('Frequency [Hz]')
        ax.set_ylabel('Modulus')
        pl.tight_layout()
        ax.legend(loc='best')


class FFT:
    """
    This is a FFT object.
    It contains the frequency, spec, modulus and phase arrays.
    It can be used for any kind of variable
    """

    def __init__(self, freq, spec):
        self.fft_freq = freq
        self.fft_spec = spec
        self.fft_mod = np.abs(spec)
        self.fft_phase = np.angle(spec)

    def plot_self(self):
        pl.plot(self.fft_freq, self.fft_spec)


class ProbeMeanHeatRealise:
    def __init__(self, avbp_bin_path=None, zpf=1):
        # Paths
        self.avbp_bin_path = avbp_bin_path

        # Attributes
        self.file_name = 'HR_mean.ascii'
        self.zpf = zpf

        # List of variables considered
        self.var_list = ['tempo', 'hr_mean']

        # time domain attributes
        self.td = dict(zip(self.var_list, [None] * len(self.var_list)))

        # frequency domain attributes
        self.fd = dict(zip(self.var_list, [None] * len(self.var_list)))

        # -------------------------
        # Reading the data from file
        self.read_data()

        # Computing frequency domain variables
        self.fill_fd_data()

    def read_data(self):
        data = np.loadtxt(self.avbp_bin_path + 'HR_mean.ascii', skiprows=1)
        data = np.array(data)

        # Storing all data in object
        for i, var in enumerate(self.var_list):
            self.td[var] = data[:, i]

    def fill_fd_data(self):
        for var in self.var_list:
            freq, spec = self.get_fft(var_name=var, zpf=self.zpf)
            self.fd[var] = FFT(freq=freq, spec=spec)

    def get_fft(self, var_name, zpf=1):
        t = self.td['tempo']
        var = self.td[var_name]

        # Sampling rate
        ts = t[1] - t[0]

        # Size of signal
        n_fft = int(t.size * zpf)

        # Hanning correction --> 2.*
        var = 2. * var * np.hanning(len(var))

        # get spectrum
        spec = np.fft.rfft(var, n=n_fft)

        # Normalize properly
        spec /= len(t)

        if False:
            # One sided spectrum ==> spec * 2.
            spec *= 2.

        # get frequency array
        freq = np.fft.rfftfreq(int(zpf * t.size), d=ts)

        return freq, spec

    def plot_time_domain(self, var_name=None):
        """
        PLot in time domain of variable 'var_name'
        :param var_name: 
        :return: 
        """
        # This is a bypass of the generic code for hr_mean
        var_name = self.var_list[-1]

        fig, ax = pl.subplots(figsize=myfigsize)
        ax.semilogy(self.td['tempo'], self.td[var_name], label='%s' % var_name)
        ax.set_xlabel('Time [s]')
        pl.tight_layout()
        ax.legend(loc='best')

    def plot_frequency_domain(self, var_name=None):
        var_name = self.var_list[-1]
        fft_o = self.fd[var_name]

        fig, ax = pl.subplots(figsize=myfigsize)
        ax.plot(fft_o.fft_freq, fft_o.fft_mod, label='%s' % var_name)
        ax.set_xlabel('Frequency [Hz]')
        ax.set_ylabel('Modulus')
        pl.tight_layout()
        ax.legend(loc='best')
